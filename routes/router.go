package routes

import (
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/gorilla_morimoto/text-file-viewer/model"
)

const (
// port = ":8000" 開発用
)

var (
	port = os.Getenv("PORT")
)

func RunServer() {

	http.HandleFunc("/", model.Initialize)
	http.HandleFunc("/list", model.ListView)
	http.HandleFunc("/card", model.CardView)
	http.HandleFunc("/search", model.Search)
	http.HandleFunc("/no-file", model.NoFile)

	// templateをExecuteするとローカルのファイルへのリンクも
	// HTTPリクエストを投げてしまうみたいなのでそれをハンドルする必要があります
	// HTMLに書いてあるパスの"../"は無視するっぽい
	// src="./main.js"なら/にリクエストを投げるということ
	// http.Dir()の相対パスはこの関数を実行するファイルのある場所が起点
	http.Handle("/view/resources/", http.StripPrefix("/view/resources/", http.FileServer(http.Dir("./view/resources/"))))

	http.Handle("/favicon.ico", http.FileServer(http.Dir("./view/resources/")))

	// ログを出力したいので、ブロックされないように別のgoroutineで
	go func() {
		http.ListenAndServe(":"+port, nil)
	}()

	log.Println("DEBUG: Listening on:", strings.Trim(port, ":"))

	// 終了せずにブロックするため
	for {

	}
	defer log.Println("DEBGUG: Exit Server")
}
