# メモ

## cardで改行をうまく表示する

2018-01-11

### 問題

GoのテンプレートエンジンでHTMLにテキストを流し込むと改行がうまく適用されなかった。

### 解決策: 流し込む段階で改行文字（`\n`）を`<br>`に変換する

#### 結果

文字通り`<br>`と表示されるだけで、改行されなかった

#### さらなる対処

JSでウィンドウの読み込み時に`<br>`を`\n`に置換することで解決

#### 追記

よくよく挙動を見てみたら`<br>` -> `\n`の置換は必要ないみたいで、`innerText`を何かしらいじると勝手に改行が反映されるみたい

## html/templateを使う場合のパスの記述について

2018-01-12

基本的に`実行ファイル`の場所からの相対パスで書けばいいみたい

```txt
- main.go
- view/
  - main.tmpl
  - executeTmpl.go
- resources/
  - main.css
  - main.js
```

このような場合、テンプレートを`Execute`する関数が`view/executeTmpl.go`だったとしても`main.tmpl`のJSを読み込む`<script>`タグには`src=./resources/main.js`と書く

また、CSSを読むには`/resources/`に対するリクエストをローカルのファイルシステムの`resources/`につなぐためのHandlerを記述しなくてはならない。

基本的には、[この記事](https://qiita.com/Sekky0905/items/fca9d9118ef23bf24791)と、参考リンクにあげられているStackOverflowの記事のとおりにやればよい。

## sync.WaitGroupの初期化について

2018-01-13

これの初期化を間違えてパニックしてしまいました。エラーメッセージは以下のとおりです。

```sh
panic: runtime error: invalid memory address or nil pointer dereference [recovered]
```

エラーの原因となっていたのは、`wg.Add(1)`でした。いろいろ試してみたところ、次のような結果となりました

```go
	var wg *sync.WaitGroup  // panic
	var wg sync.WaitGroup   // OK
	wg := &sync.WaitGroup{} // OK
```

1番目がダメで、3番めが大丈夫な理由があまり直感的ではなかったのですが、まとめると以下のような感じかと思われます。

1. wgは`*WaitGroup`の変数。ただし、初期化されていないため、値はポインタ型のデフォルト値の`nil`（そのためパニック）
2. wgは`WaitGroup`の変数。デフォルト値で初期化される
3. wgは`*WaitGroup`の変数。ただし、`sync.WaitGroup{}`によって作成された`WaitGroup`の値のポインタなので`nil`ではない。

ということだと思われます。

ちなみに、`Add()`のレシーバーは`*WaitGroup`でしたので、2の方法で初期化するのは厳密には正しくないっぽいです。Goだと暗黙的にレシーバーのポインタと値を変換してくれるため、こちらでもうまくいくっぽいですが。

[香り屋さんの記事](https://www.kaoriya.net/blog/2013/07/08/)では`var wg sync.WaitGroup`を使っていて、[ruiuさんの記事](https://qiita.com/ruiu/items/dba58f7b03a9a2ffad65)では`wg := &sync.WaitGroup{}`を使っていました。

## キャプチャをgifにする

1. Chromeの拡張機能でキャプチャ
2. `ffmpeg`でmp4 -> gif

### コマンド

```cmd
ffmpeg -t 10 -an -i input.mp4 output.gif
```

オプション

```cmd
-t: 動画の時間
-ss: 開始時間
-an: 出力に音声を含めない
-i: ファイル名指定
```

[参考ページ](https://qiita.com/takuya-ki/items/13e445096752b8181de7)

## CSSだけで、文字あふれのときに...で省略する

`card-action`にパスを表示させたらあふれてしまったので、`...`で省略する方法を探しました。

以下のとおりにCSSを設定すると良いそうです。

```css
p{
  width: 100px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -o-text-overflow: ellipsis; /* Opera9,10対応 */
}
```

今回はOperaに対応するつもりはないのでそこは書かず、また、`width`は`100%`として実装してあります。

[参考ページ](https://qiita.com/ichikawa_0829/items/e0c364e74b5e22a7bc0d)

## filepath.Walk()

使ったのははじめてではないんですが、こいつの便利さに改めて感動しました。ローカルのファイルに対して一括して処理をする、みたいなときは`file.Readdirnames()`で名前を取得して～じゃなくてこれで一発ですね。

## なぜhttp.ResponseWriterは関数に渡すときに値を渡すのに、http.Requestはポインタを渡すのか

まずは[こちら](https://stackoverflow.com/questions/13255907/in-go-http-handlers-why-is-the-responsewriter-a-value-but-the-request-a-pointer)をご覧ください

現在（2018-01-19 2:49）一番上の回答はこちらになります。

> What you get for `w` is a pointer to the non exported type `http.response` but as `ResponseWriter` is an interface, that's not visible.

ざっくりいうと、「`w`はそもそもポインタなんだよ」という感じでしょうか。

じゃあ具体的なデータ構造はどうなっているのかというと、Goの公式サイトには文書がありませんでした（探せなかっただけで存在する可能性はありますが）。

いろいろ調べてみると、[astaxieさんの記事](https://astaxie.gitbooks.io/build-web-application-with-golang/ja/02.6.html)と[Russ Coxさんのブログの記事](https://research.swtch.com/interfaces)を見つけて、これを読んだらなんとなくわかったのでメモしておきます。

まず、GoのInterface Valueはtwo-wordの値で、ひとつのwordはInterfaceに紐付けられたメソッドテーブル（たぶん、このInterfaceを実装するにはこれらのメソッドを持ってね、という表のことだと思う）へのポインタで、もう片方のwordは関連するデータへのポインタです。

wordはCPUのbit数のことっぽいです。一回にCPUが処理できるbit数。

[参考: コンピュータ内部でのデータの表現 (1) ビット，バイト，ワードの概念](http://www.aoni.waseda.jp/ichiji/2011/is-03/is-02-2.html)

この、「関連するデータ (associated data)」というのがいまいち腑に落ちなかったのですが、astaxieさんの記事を見て納得できました。

Interface Valueというのはそもそも異なる型を持ちえます。たとえば、以下のような状況だとMen Interface型の値は`Human`, `Employee`, `Student`型になりえます(変数`j`はこの3つのどの型でも代入できる)。

```go
type Human struct {
  name string
}

func (i Human) Speak() {
  fmt.Println("hi")
}

type Employee struct {
  Human
  salary int
}

func (i Employee) Speak() {
  fmt.Println("hey")
}

type Student struct {
  Human
  major sting
}

func (i Student) Speak() {
  fmt.Println("hello")
}

type Men interface {
  Speak()
}

var j Men
```

つまり、Interfaceは2つのポインタが合わさった型なので、http.ResponseWriterはポインタではなく値でいいのだと理解しました。

もちろん合っているか、正確かどうかはわかりませんが、だいたいいい線いっているのではないかと思います。

## <form>, GETメソッドで値を送信できなかった

2018-01-21

`card.html`に検索ボックスを実装している途中なのですが、GETで値を送信するのにはまってしまったのでメモしておきます。

当初は、materializeのSearch Barのサンプルコードをそのまま持ってきていましたが、値を送信するとURLが`localhost:8000/search?`となってしまっていて、フォームに入力した値が送信できていませんでした。

```html
  <nav>
    <div class="nav-wrapper">
      <form>
        <div class="input-field">
          <input id="search" type="search" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>
```

原因は`<input>`で`name=`を設定していなかったことでした。ここで、`<name>=query`としてあげると、フォームが`/localhost:8000/search?q=hoge`となって、値を送信することができます。

## 404をハンドリングする

https://stackoverflow.com/questions/9996767/showing-custom-404-error-page-with-standard-http-package

リクエストされたパスに対して、ハンドラーが設定されていなかった場合、Goのhttpサーバーは`/`にそのリクエストを投げます。

つまり、not foundとなるべきパスも`/`につながれてしまうので、それをハンドルする必要があるみたいです。

なんかの記事でそう書いてあった気がしたんですが、どこで見たのかまったく思い出せません。たしか英語で、公式のページだったような気がするんですが……。めちゃくちゃ気持ち悪いです。どこで見たんだっけか……。

### 追記

見つかりました！

[ServeMux](https://golang.org/pkg/net/http/#ServeMux)

やっぱり、公式で英語でした。

> Note that since a pattern ending in a slash names a rooted subtree, the pattern "/" matches all paths not matched by other registered patterns, not just the URL with Path == "/".

そのものズバリでここですね。

## TODO: sliceに値が含まれるかどうかの確認

https://stackoverflow.com/questions/10485743/contains-method-for-a-slice

を見ると、`contains()`の実装は簡単だけど、それより`map`を使ってみたら？という指摘