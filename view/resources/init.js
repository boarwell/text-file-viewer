// 即時実行関数の代わりにブロックで変数の範囲を指定しました
{
  'use strict';

  const elems = document.querySelectorAll('.modal');
  // なぜかわからないけど、これでいけた
  // instanceは上書きされちゃうけどこれでいいの？？？
  let instance = null;
  for (elem of elems) {
    instance = M.Modal.init(elem);
  }
}


window.onload = () => {
  // pタグのテキスト内の<br>を改行に置換する
  const elems = document.getElementsByTagName('p');
  for (elem of elems) {
    // よく理屈がわかってないけど、innerTextに対して何かしらの処理をすると改行が有効になるみたいなので
    elem.innerText = elem.innerText.replace(/hoge/g, 'hoge');
  }

}
// console.log(instance); -> ブロック外なのでinstanceは定義されておらず、エラーになる