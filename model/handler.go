package model

import (
	"html/template"
	"log"
	"net/http"
	"strings"
)

const (
	card    = "./view/card.html" // テンプレートファイルの場所は実行ファイルの場所からのパスを記述する
	list    = "./view/list.html"
	noFiles = "./view/no-file.html"
	target  = "./dev/" //対象ディレクトリは実行ファイルの場所からのパスを記述する
)

func ExecuteTmpl(w http.ResponseWriter, tmplPath string, f []File) error {
	log.Println("DEBUG: Start Parsing", tmplPath)

	t, err := template.ParseFiles(tmplPath)
	if err != nil {
		log.Println("ERROR: Failed to parse", tmplPath)
		return err
	}

	t.Execute(w, f)
	return nil
}

func Initialize(w http.ResponseWriter, r *http.Request) {
	log.Println("DEBUG: model.Initialize() called")
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	f := Files(target)
	if f == nil {
		log.Printf("ERROR: it seems there's no files in %[1]s, or there's no directory named %[1]s\n", target)
		http.Redirect(w, r, "/no-file", 303)
		return
	}

	log.Printf("DEBUG: Request from: %s", r.RemoteAddr)
	ExecuteTmpl(w, card, f) // TODO: errが返ってきたときに表示するためのHTML（エラー画面を作る）
}

func ListView(w http.ResponseWriter, r *http.Request) {
	log.Println("DEBUG: Request from:", r.RemoteAddr)
	ExecuteTmpl(w, list, Files(target))
}

func CardView(w http.ResponseWriter, r *http.Request) {
	log.Println("DEBUG: Request from:", r.RemoteAddr)
	ExecuteTmpl(w, card, Files(target))
}

func Search(w http.ResponseWriter, r *http.Request) {
	f := Files(target)
	var hit []File
	q := r.FormValue("q")

	for _, v := range f {
		if strings.Contains(v.FileName, q) {
			hit = append(hit, v)
		} else if strings.Contains(v.Contents, q) {
			hit = append(hit, v)
		} else if strings.Contains(v.Path, q) {
			hit = append(hit, v)
		}
	}
	ExecuteTmpl(w, card, hit)
}

func NoFile(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles(noFiles))
	t.Execute(w, target)
}
