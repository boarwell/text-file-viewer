package model

import (
	"io/ioutil"
	"os"
	"testing"
)

var (
	testpath = "./test"
	text0    = []byte("hoge is not hogera")
)

func createTmpDir() {
	os.Mkdir("test", 0755)
	ioutil.WriteFile("./test/test0.txt", text0, 0755)
}

func TestFiles(t *testing.T) {
	createTmpDir()

	f := Files(testpath)

	if f[0].Contents != string(text0) {
		t.Errorf("the contents of %s is not expected value -> %s\n", f[0].FileName, f[0].Contents)
	}

	os.RemoveAll("./test/")
}

func TestMkDigest(t *testing.T) {
	var b0 []byte
	if mkDigest(b0) != "" {
		t.Error("failed to convert empty string")
	}

	t1 := "length is < 100"
	b1 := []byte(t1)
	if mkDigest(b1) != t1 {
		t.Errorf("failed to conbert \"%s\"", t1)
	}

	// 100 chars
	t2 := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et tincidunt est. Sed et luctus amet."
	b2 := []byte(t2)
	if mkDigest(b2) != t2 {
		t.Error("failed to convert 100 char-string")
	}

	// 101 chars
	t3 := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque condimentum lectus sed molestie sed."
	b3 := []byte(t3)
	if mkDigest(b3) != t3[:100] {
		t.Log(mkDigest(b3))
		t.Error("failed to convert 101 char-string")
	}

}

func TestContains(t *testing.T) {
	sl := []string{".txt", ".md"}
	if !contains(sl, ".txt") {
		t.Errorf("failed: .txt is in slice")
	}
	if contains(sl, ".hoge") {
		t.Errorf("failed: .hoge is NOT in slice")
	}
}
